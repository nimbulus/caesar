﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Xml;
using System.Net;
namespace rat
{
    public class Utilities
    {
        public Utilities()
        {
        }
        public string CreateMD5(string input)
        {
            // Use input string to calculate MD5 hash
            using (System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create())
            {
                byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
                byte[] hashBytes = md5.ComputeHash(inputBytes);

                // Convert the byte array to hexadecimal string
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < hashBytes.Length; i++)
                {
                    sb.Append(hashBytes[i].ToString("X2"));//to hexadecimal, number of digits per integer
                }
                return sb.ToString();
            }
        }
        public List<string> SplitResponse(string input, string nodeOfInterest)
        {
            input = "<parent>" + input + "</parent>"; //super hacky way to sidestep the wierd "whole document" thing C# imposes
            XmlDocument xmlDoc = new XmlDocument();
            List<string> retstring = new List<string>();
            xmlDoc.LoadXml(input);
            foreach (XmlNode xmlNode in xmlDoc.DocumentElement.ChildNodes)
            {
                if(xmlNode.Name == nodeOfInterest)
                {
                    Console.WriteLine(xmlNode.InnerText);
                    retstring.Add(xmlNode.InnerText);
                }

            }

            return retstring;
        }
        public Dictionary<string,string> GetSystemInfo()
        {
            Dictionary<string, string> sysinfo = new Dictionary<string, string>();

            string runas = System.Security.Principal.WindowsIdentity.GetCurrent().Name;// who Im running as
            string userName = Environment.UserName; //who I'm logged in as
            string HostName = Dns.GetHostName();
            string arch = "x86 compatible";
            string OperatingSystem = Environment.OSVersion.ToString();
            string firstMacAddress = GetMacAddress();
            string cwd = System.Environment.CurrentDirectory;
            sysinfo.Add("runas", runas);
            sysinfo.Add("username", userName);
            sysinfo.Add("hostname", HostName);
            sysinfo.Add("arch", arch);
            sysinfo.Add("os", OperatingSystem);
            sysinfo.Add("mac", firstMacAddress);
            sysinfo.Add("cwd", cwd);
            return sysinfo;
        }
        private string GetMacAddress()
        {
            string macAddresses = string.Empty;

            foreach (System.Net.NetworkInformation.NetworkInterface nic in System.Net.NetworkInformation.NetworkInterface.GetAllNetworkInterfaces())
            {
                if (nic.OperationalStatus == System.Net.NetworkInformation.OperationalStatus.Up)
                {
                    macAddresses += nic.GetPhysicalAddress().ToString();
                    break;
                }
            }

            return macAddresses;
        }
    }
}
